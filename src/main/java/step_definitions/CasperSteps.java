package step_definitions;

import PageObjectModule.Commonfns;
import cucumber.api.java8.En;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class CasperSteps implements En {

    public CasperSteps() throws IOException {
        FileReader reader=new FileReader("src/main/java/PageObjectModule/property.properties");
        Properties read=new Properties();
        read.load(reader);
        String casperid=read.getProperty("casperID");
        String password=read.getProperty("password");

        Given("^Login to applicationa$", () -> {
            Commonfns.loginPage("IXE0865","78350619");
        });
        When("^Create new patienta$", () -> {
            Commonfns.createPateint();
        });
        Then("^logout successfullya$", () -> {
            Commonfns.signOut();
            Commonfns.browserQuit();
        });
        Given("^Login with New Patienta$", () -> {
            //Commonfns.deleteProperties();
            Commonfns.loginPage(casperid,password);
        });
        When("^Verify the T&C And MedicalInfoa$", () -> {
            Commonfns.VerifyTermsAndConditionsPage();
        });
        Then("^Logout successfullya$", () -> {
            Commonfns.deleteProperties();
           Commonfns.browserQuit();
        });
    }
}
