package PageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class Commonfns {
    private static WebDriver driver;
    @FindBy(how= How.XPATH, using="(//*[contains(text(),\"Login\")])[1]")
    static WebElement loginSubmit;
    @FindBy(how=How.CSS,using="#mat-input-0")
    static WebElement loginID;
    @FindBy(how=How.CSS,using="#mat-input-1")
    static WebElement loginPassword;
    @FindBy(how=How.XPATH,using="//*[contains(text(),\"Add Patient\")]")
    static WebElement addPatient;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"first_name\"]")
    static WebElement firstName;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"last_name\"]")
    static WebElement lastName;
    @FindBy(how=How.XPATH,using="//*[@aria-label=\"Day\"]")
    static WebElement selectDay;
    @FindBy(how=How.XPATH,using="//*[@aria-label=\"Month\"]")
    static WebElement selectMonth;
    @FindBy(how=How.XPATH,using="//*[@aria-label=\"Year\"]")
    static WebElement selectYear;
    @FindBy(how=How.XPATH,using="//*[@class=\"mat-form-field-infix\"]//following::span[contains(text(),\"Gender\")]")
    static WebElement selectGender;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"email\"]")
    static WebElement emailID;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"phone\"]")
    static WebElement phone;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"street\"]")
    static WebElement address;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"zip\"]")
    static WebElement zipCode;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"city\"]")
    static WebElement city;
    @FindBy(how=How.XPATH,using="//*[@formcontrolname=\"country_id\"]")
    static WebElement clickCountry;
    @FindBy(how = How.XPATH,using = "//*[contains(text(),\"SAVE\")]")
    static WebElement clickSave;
    @FindBy(how=How.XPATH,using = "//*[@role=\"dialog\"]//following::div[@class=\"subtitle\"]")
    static WebElement confirmationMsg;
    @FindBy(how=How.XPATH,using="(//div[contains(text(),\"Caspar ID\")]/..//div)[2]")
    static WebElement getNewCasperID;
    @FindBy(how=How.XPATH,using = "(//div[contains(text(),\"Temporary Password\")]/..//div)[2]")
    static WebElement getNewPassword;
    @FindBy(how=How.XPATH,using="//*[contains(text(),\"Close\")]")
    static WebElement popUpClose;
    @FindBy(how=How.XPATH,using="//*[contains(text(),\"release of medical information.\")]")
    static WebElement releaseMedicalInformation;
    @FindBy(how=How.XPATH,using="//*[contains(text(),\"terms and conditions\")]")
    static WebElement termsAndConditions;
    @FindBy(how=How.XPATH,using="//*[@id=\"mat-checkbox-1\"]")
    static WebElement checkBoxOfTAndC;


    public static void beforeTest()
    {
        WebDriverManager.getInstance(CHROME).setup();
        DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
        acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
        driver = new ChromeDriver( acceptSSlCertificate );
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        PageFactory.initElements(driver,Commonfns.class);
    }
    public static void loginPage(String id,String pwd){
        beforeTest();
        driver.get("https://beta.caspar-health.com/en/#/user/sign_in");
        //driver.findElement(By.cssSelector("#mat-input-0")).sendKeys(id);
        //driver.findElement(By.cssSelector("#mat-input-1")).sendKeys(pwd);
        //driver.findElement(By.xpath("(//*[contains(text(),\"Login\")])[1]")).click();
        loginID.sendKeys(id);
        loginPassword.sendKeys(pwd);
        loginSubmit.click();
    }


    public static void createPateint() throws IOException {
     WaitforPageToLoad("//*[contains(text(),\"Add Patient\")]");
     addPatient.click();
     WaitforPageToLoad("//*[@class=\"simple-header-title ng-star-inserted\"]");
     firstName.sendKeys("Sai");
     lastName.sendKeys("SaiVinoth");
     dobSelection("21","June","1988");
     genderSelection("Male");
     emailID.sendKeys("chintu7@gmail.com");
     phone.sendKeys("65635126773");
     address.sendKeys("12 bolschstrasse berlin");
     zipCode.sendKeys("12345");
    city.sendKeys("Berlin");
    clickCountry.click();
    driver.findElement(By.xpath("//*[@role=\"option\"]/..//*[contains(text(),\"Germany\")]")).click();
    clickSave.isEnabled();
    clickSave.click();
    WaitforPageToLoad("//*[@role=\"dialog\"]//following::div[@class=\"subtitle\"]");
    String getID= getNewCasperID.getText();
    String getPassword=getNewPassword.getText();
    System.out.println("hello world"+getID+getPassword);
    writeProperties(getID,getPassword);
    popUpClose.click();
       }



    public static void WaitforPageToLoad(String path){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath(path)));
    }


    public static void dobSelection(String day,String month,String year) {
        selectDay.click();
        WebElement daySelect=driver.findElement(By.xpath("//*[@role=\"option\"]/..//span[contains(text(),"+day+")]"));
        daySelect.click();
        selectMonth.click();
        WebElement monthSelect=driver.findElement(By.xpath("//*[@role=\"option\"]/..//span[contains(text(),"+month+")]"));
        monthSelect.click();
        selectYear.click();
        WebElement yearMonth=driver.findElement(By.xpath("//*[@role=\"option\"]/..//span[contains(text(),"+year+")]"));
        yearMonth.click();
    }
    public static void genderSelection(String gender){
        selectGender.click();
        driver.findElement(By.xpath("//*[@role=\"option\"]/..//span[contains(text(),"+gender+")]")).click();
    }
    public static void writeProperties(String id,String pwd) throws IOException {
        System.out.println("yes got it"+id+pwd);
        FileOutputStream write=new FileOutputStream("src/main/java/PageObjectModule/property.properties");
        Properties getProp=new Properties();
        getProp.setProperty("casperID",id);
        getProp.setProperty("password",pwd);
        getProp.store(write,null);
    }

    public static void readProperties(String id,String pwd) throws IOException {
        FileReader reader=new FileReader("src/main/java/PageObjectModule/property.properties");
        Properties read=new Properties();
        read.load(reader);
        String casperid=read.getProperty("casperID");
        String password=read.getProperty("password");
        System.out.println(read.getProperty("casperID"));
        System.out.println(read.getProperty("password"));

    }
    public static void deleteProperties() throws IOException {
        FileOutputStream reader=new FileOutputStream("src/main/java/PageObjectModule/property.properties");
        Properties delete=new Properties();
        delete.clear();
        delete.store(reader,null);
    }
    public static void signOut(){
        driver.findElement(By.xpath("//*[contains(text(),\"Sign out\")]")).click();
    }

    public static void VerifyTermsAndConditionsPage(){
    releaseMedicalInformation.isDisplayed();
    termsAndConditions.isDisplayed();
    checkBoxOfTAndC.isDisplayed();
    }
    public static void browserQuit(){
    driver.quit();
    }
}